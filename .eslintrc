parser: babel-eslint
parserOptions:
  ecmaVersion: 2016
  sourceType: module
env:
  es6: true
  browser: false
  node: true
  mongo: true
  jest: true
globals:
  http: true
plugins:
  - import
  - node
  - promise
  - standard
extends:
  - node
  - esnext
rules:
  accessor-pairs: error
  array-callback-return: error
  arrow-spacing: [error, {before: true, after: true}]
  block-scoped-var: error
  block-spacing: [error, always]
  brace-style: [error, 1tbs, {allowSingleLine: true}]
  camelcase: [error, {properties: never}]
  class-methods-use-this: error
  comma-dangle: [error, {arrays: never, objects: never, imports: never, exports: never, functions: never}]
  comma-spacing: [error, {before: false, after: true}]
  comma-style: [error, last]
  consistent-return: error
  constructor-super: error
  curly: [error, multi-line]
  default-case: error
  dot-location: [error, property]
  eol-last: error
  eqeqeq: [error, always, {'null': ignore}]
  func-call-spacing: [error, never]
  generator-star-spacing: [error, {before: true, after: true}]
  global-require: error
  handle-callback-err: [error, ^(err|error)$]
  import/export: error
  import/first: error
  import/no-absolute-path: error
  import/no-duplicates: error
  import/no-nodejs-modules: 0
  import/no-webpack-loader-syntax: error
  indent: [error, 2, {SwitchCase: 1}]
  init-declarations: [error, always]
  key-spacing: [error, {beforeColon: false, afterColon: true}]
  keyword-spacing: [error, {before: true, after: true}]
  new-cap: [error, {newIsCap: true, capIsNew: false}]
  new-parens: error
  no-alert: error
  no-array-constructor: error
  no-caller: error
  no-class-assign: error
  no-cond-assign: error
  no-console: warn
  no-const-assign: error
  no-constant-condition: [error, {checkLoops: false}]
  no-control-regex: error
  no-debugger: error
  no-delete-var: error
  no-dupe-args: error
  no-dupe-class-members: error
  no-dupe-keys: error
  no-duplicate-case: error
  no-empty-character-class: error
  no-empty-function: error
  no-empty-pattern: error
  no-empty: error
  no-eq-null: error
  no-eval: error
  no-ex-assign: error
  no-extend-native: error
  no-extra-bind: error
  no-extra-boolean-cast: error
  no-extra-label: error
  no-extra-parens: [error, functions]
  no-extra-semi: error
  no-fallthrough: error
  no-floating-decimal: error
  no-func-assign: error
  no-global-assign: error
  no-implied-eval: error
  no-inner-declarations: [error, functions]
  no-invalid-regexp: error
  no-invalid-this: 0
  no-irregular-whitespace: error
  no-iterator: error
  no-label-var: error
  no-labels: [error, {allowLoop: false, allowSwitch: false}]
  no-lone-blocks: error
  no-mixed-operators: [error, {groups: [['==', '!=', '===', '!==', '>', '>=', '<', '<='], ['&&', '||'], [in, instanceof]], allowSamePrecedence: true}]
  no-mixed-spaces-and-tabs: error
  no-multi-spaces: error
  no-multi-str: error
  no-multiple-empty-lines: [error, {max: 1, maxEOF: 0}]
  no-negated-in-lhs: error
  no-new-func: error
  no-new-object: error
  no-new-require: error
  no-new-symbol: error
  no-new-wrappers: error
  no-new: error
  no-obj-calls: error
  no-octal-escape: error
  no-octal: error
  no-path-concat: error
  no-proto: error
  no-redeclare: error
  no-regex-spaces: error
  no-return-assign: [error, except-parens]
  no-return-await: error
  no-self-assign: error
  no-self-compare: error
  no-sequences: error
  no-shadow-restricted-names: error
  no-shadow: [error,  { builtinGlobals: false, hoist: functions, allow: [done] }]
  no-sparse-arrays: error
  no-tabs: error
  no-template-curly-in-string: error
  no-this-before-super: error
  no-throw-literal: error
  no-trailing-spaces: error
  no-undef-init: error
  no-undef: error
  no-unexpected-multiline: error
  no-unmodified-loop-condition: error
  no-unneeded-ternary: [error, {defaultAssignment: false}]
  no-unreachable: error
  no-unsafe-finally: error
  no-unsafe-negation: error
  no-unused-expressions: [error, {allowShortCircuit: true, allowTernary: true}]
  no-unused-vars: [warn, {vars: all, args: none, ignoreRestSiblings: true}]
  no-use-before-define: [error, {functions: false, classes: false, variables: false}]
  no-useless-call: error
  no-useless-computed-key: error
  no-useless-constructor: error
  no-useless-escape: error
  no-useless-rename: error
  no-useless-return: error
  no-whitespace-before-property: error
  no-with: error
  node/no-deprecated-api: error
  node/process-exit-as-throw: error
  object-property-newline: [error, {allowMultiplePropertiesPerLine: true}]
  one-var: [error, {initialized: never}]
  operator-linebreak: [error, after, {overrides: {'?': before, ':': before}}]
  padded-blocks: [error, {blocks: never, switches: never, classes: never}]
  prefer-promise-reject-errors: error
  promise/param-names: error
  quotes: [error, single, {avoidEscape: true, allowTemplateLiterals: true}]
  rest-spread-spacing: [error, never]
  semi-spacing: [error, {before: false, after: true}]
  semi: [error, always]
  space-before-blocks: [error, always]
  space-before-function-paren: [error, {named: never, anonymous: always}]
  space-in-parens: [error, never]
  space-infix-ops: error
  space-unary-ops: [error, {words: true, nonwords: false}]
  spaced-comment: [error, always, {line: {markers: ['*package', '!', /, ',']}, block: {balanced: true, markers: ['*package', '!', ',', ':', '::', flow-include], exceptions: ['*']}}]
  standard/array-bracket-even-spacing: [error, either]
  standard/computed-property-even-spacing: [error, even]
  standard/no-callback-literal: error
  standard/object-curly-even-spacing: [error, either]
  strict: [error, global]
  symbol-description: error
  template-curly-spacing: [error, never]
  template-tag-spacing: [error, never]
  unicode-bom: [error, never]
  use-isnan: error
  valid-jsdoc: error
  valid-typeof: [error, {requireStringLiterals: true}]
  wrap-iife: [error, any, {functionPrototypeMethods: true}]
  yield-star-spacing: [error, both]
  yoda: [error, never]
