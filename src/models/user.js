import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
import isEmail from 'validator/lib/isEmail';

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true,
    validate: [isEmail, 'No valid email address provided.']
  },
  password: {
    type: String,
    required: true,
    minlength: 7,
    maxlength: 42
  },
  role: {
    type: String
  }
});

userSchema.statics.findByLogin = async function (login) {
  let user = await this.findOne({
    username: login
  });

  if (!user) {
    user = await this.findOne({ email: login });
  }

  return user;
};

userSchema.pre('remove', function (next) {
  const user = this;
  user.model('Message').deleteMany({ userId: user._id }, next);
});

userSchema.pre('save', async function () {
  const user = this;
  user.password = await user.generatePasswordHash();
});

userSchema.methods.generatePasswordHash = async function () {
  const saltRounds = 10;
  const user = this;
  const result = await bcrypt.hash(user.password, saltRounds);
  return result;
};

userSchema.methods.validatePassword = async function (password) {
  const result = await bcrypt.compare(password, this.password);
  return result;
};

const User = mongoose.model('User', userSchema);

export default User;
