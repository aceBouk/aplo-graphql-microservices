## Installation

* `git clone https://gitlab.com/aceBouk/aplo-graphql-microservices`
* `cd aplo`
* `touch .env`
* `npm install`
* fill out *.env file* (see below)
* `npm start`
* [start MongoDB]
* visit `http://localhost:8000` for GraphQL playground

#### .env file

 [TODO: write setup tutorial]. After you have created a MongoDB database, you can fill out the environment variables in the *server/.env* file.

```
SECRET=asdlplplfwfwefwekwself.2342.dawasdq

DATABASE_URL=mongodb://localhost:27017/mydatabase
```

The `SECRET` is just a random string for your authentication. Keep all these information secure by adding the *.env* file to your *.gitignore* file. No third-party should have access to this information.

#### Testing

* adjust `test:run-server` npm script with `TEST_DATABASE_URL` environment variable in package.json to match your testing database name
* one terminal: npm run test:run-server
* second terminal: npm run test:execute-test
